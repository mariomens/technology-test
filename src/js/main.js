"use strict";

let widget = document.querySelector(".task-widget");
let widjetOptions = {};
let task = new TaskWidjet(widget);

function TaskWidjet(obj) {
    TaskWidjet.hex = "asdasd";
    this.widget = obj;
    this.filters = obj.querySelectorAll(".task-widget__header-sorting");
    this.activeFilters = [];
    this.activeFilters.length = this.filters.length;
    activateSelectMenu(this.filters, this.activeFilters);
    addSortFunc(this.filters, this.widget);
    
    
    this.widgetBodyList = this.widget.querySelector(".task-widget__body-list");
    this.createComponentForm = this.widget.querySelector(".task-widget__filter");
    this.componentFormValues = { 
        id: this.createComponentForm.querySelector("input.task-widget__form-input[data-create='id']"),
        number: this.createComponentForm.querySelector("input.task-widget__form-input[data-create='number']"),
        time: this.createComponentForm.querySelector("input.task-widget__form-input[data-create='time']"),
        timeEnd: this.createComponentForm.querySelector("input.task-widget__form-input[data-create='timeEnd']"),
        type: this.createComponentForm.querySelector("ul.task-widget__header-sorting[data-create='type']"),
        errorPopup:  this.widget.querySelector(".task-widget__popup.task-widget__popup-form"),
    }
    addMask(this.componentFormValues.time, "__.__._____");
    addMask(this.componentFormValues.timeEnd, "__.__._____");
    addMask(this.componentFormValues.number, "____");
    //addMask(this.componentFormValues.id, "AA", "string");
    this.componentFormValues.errorPopup.addEventListener("click", function(){
        this.classList.toggle("active");
    })

    window.addEventListener("load", ()=> {
        this.componentFormValues.id.value = generetaID();
    })

    this.componentSerialNumberStorage = [0]; //для хранения порядковых номеров id
    this.createComponentButton = this.widget.querySelector(".task-widget__header-add");
    this.createComponentButton.addEventListener("click", () => {
        createComponent(this.createComponentForm, this.widgetBodyList, 
            this.componentFormValues, this.componentSerialNumberStorage);
    })

    const LOCAL_STORAGE_NAME = "widjetComponents";
    window.addEventListener("load", (event) => {
        createComponentFromStorage(this.widgetBodyList, this.componentSerialNumberStorage);
    });

    window.addEventListener('beforeunload', (event) => {
        saveComponentInStorage(this.widgetBodyList);
    });

    // привязка события сброса DragAndDrop Компонента
    this.widgetBodyList.addEventListener("dragover", (event)=> {
        onDragOver(event)
    })
    this.widgetBodyList.addEventListener("drop", (event) =>{
        onDrop(event,this.widgetBodyList);
    })
    // запреить/разрешить перетаскивание компонентов
    this.dragDropCheckbox = this.widget.querySelector("#drag-drop");
    this.dragDropCheckbox.addEventListener("change",
        dragDropPermissionHandler
        .bind(this.dragDropCheckbox,this.widgetBodyList)
    )

    function generetaID() {
        let id = Date.now();
        id = String(id).match(/.{1,2}/g);
        for(let i=0; i < id.length; i++) {
            if ( (id[i] > 97) && (id[i] < 123) ) id[i] = String.fromCharCode(id[i]);
            if ( (id[i] > 64) && (id[i] < 91) ) id[i] = String.fromCharCode(id[i]);
            if ( (id[i] > 47) && (id[i] < 58) ) id[i] = String.fromCharCode(id[i]);
        }
        id = id.join("");
        return id;
    }

    function activateSelectMenu(selectMenuList, arraySelectStatus) {
        for (let i=0; i < selectMenuList.length; i++) {
            selectMenuList[i].addEventListener("click", () => {
                selectMenuList[i].classList.toggle("active");
                arraySelectStatus[i] = selectMenuList[i].classList.contains("active");
            })
        }

        document.addEventListener("mouseup", (e) => { 
            for (let i=0; i < selectMenuList.length; i++) {
                let isMenuOpen = arraySelectStatus[i];

                if (isMenuOpen) {
                    let arr = e.target.classList.value.split(" "); //Если не будет внутри элемента класса не сработает
                    arr[0] = "." + arr[0];
                    let isIncludesMenu = selectMenuList[i].querySelector(arr[0])? true: false;
                    if (!isIncludesMenu) {
                        selectMenuList[i].classList.remove("active");
                    }
                } else {
                    continue;
                }
            }
        }, false);
    }

   function addSortFunc(filterList, widjet) {
        for (const filter of filterList) {
            if (filter.dataset.filter === "order-filter"){ //Если список имеет дата атрибут, то по его элеентам будет сортировка
                sortFunction(filter, widjet);
            } else if (filter.dataset.filter === "order-type") {
                changeFollowind(filter, widjet);
            }
        }

        
    }

    function sortFunction(filter, widjet) {
        let widgetBody = widjet.querySelector(".task-widget__body-list");
        let filterParameters = filter.querySelectorAll("li.header-sorting__li");

        for (const parameter of filterParameters) {
            parameter.addEventListener("click", sortCard) // Привязка алгоритма сортировки элементам списка
        }


        function sortCard() {
            let parentList = this.parentElement;
            if (parentList.classList.contains("active")) {
                let sortByParam = this.dataset?.filter?.split("-");

                if (sortByParam !== undefined){ // Если дата-аттрибут не пустой, то сортируем

                    let widjetItemList = widgetBody.querySelectorAll(".task-widget__item");
                    let findRequest = `.task-widget__item-body li[data-${sortByParam[0]}]`;

                    for(let i=0; i < widjetItemList.length; i++) { //Сортировки элементов
                        
                        for(let k = (i+1); k < (widjetItemList.length); k++) {
                            
                            let findProperty = widjetItemList[i].querySelector(findRequest).dataset[sortByParam[0]];
                            let findPropertyNext = widjetItemList[k].querySelector(findRequest).dataset[sortByParam[0]];
/* Для дата в формате дд.мм.гггг
                            if (sortByParam[0] === "date") { //если дата, то переверни Дату
                                findProperty = findProperty.split(".").reverse().join(",").replace(/\s/g, "");
                                findPropertyNext = findPropertyNext.split(".").reverse().join(",").replace(/\s/g, "");
                                findProperty = (new Date(findProperty)).getTime(); // с милисекундами работает быстрее
                                findPropertyNext = (new Date(findPropertyNext)).getTime();
                            }
*/
                            if ( (findPropertyNext < findProperty) && (sortByParam[1] === "down") ) {
                                swapNodes(widjetItemList[i],widjetItemList[k]);
                            }
                            if ( (findPropertyNext > findProperty) && (sortByParam[1] === "up") ) {
                                swapNodes(widjetItemList[i],widjetItemList[k]);
                            }
                        }
                        //console.log(findProperty);
                    }
                }
                //parentList.prepend(this); //перемещаем выбранный элемент списка сортировки в начало списка
            }

            function swapNodes(el1, el2) {
                let childNodes1 = el1.lastElementChild;
                let childNodes2 = el2.lastElementChild; 

                el1.prepend(childNodes2);
                el2.prepend(childNodes1);
            }

        }
    }

    function changeFollowind(filter, widjet) {
        let filterParameters = filter.querySelectorAll("li.header-sorting__li");
        for (const parameter of filterParameters) {
            parameter.addEventListener("click", prependParametr) // Привязка алгоритма сортировки элементам списка
        }

        function prependParametr() {
            let parentList = this.parentElement;
            if (parentList.classList.contains("active")) {
                parentList.prepend(this);
            }
        }
    }


    function createComponent(odjectWithParameters, widgetBodyList, formElement, SerialNumberStorage) {
        let parameters = {
            id: formElement.id.value,
            number: formElement.number.value,
            timeFrom: formElement.time.value.replace(/\s/g, ""),
            timeTo: formElement.timeEnd.value.replace(/\s/g, ""),
            type: formElement.type.children[0].innerText,
            permission: true,
        }

        //if (parameters.timeFrom.length < 10) parameters.permission = false;
        /*
        for (let key in parameters) { //если нет какого-то из ключей запретить добавление компонента

            if ( (parameters[key] === null) || (parameters[key] === undefined) || (parameters[key] == "")) {
                parameters.permission = false;
            }
        } */
        if (!parameters.permission) formElement.errorPopup.classList.add("active");
         
        if (parameters.permission) createItem(widgetBodyList, parameters.id, 
            parameters.timeFrom,parameters.timeTo, parameters.number, 
            parameters.type, SerialNumberStorage);

        formElement.id.value = generetaID();
    }
    function createItem(placeToPaste, id = 0, timeFrom = "", timeTo = "", number = 0, type = "RUED", SerialNumberStorage = [0], time = false) {
        let component = document.createElement("li");
        component.classList.add("task-widget__item");
        // create nomTime
        if (time != false) {
            time = new Date(Number(time));
        } else {
            time = new Date();
        }
        let timeMilisecond = time.getTime();
        let timeToComponent = `${time.getDate()}.${time.getMonth()+1}.${time.getFullYear()} 
                                ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`;
        // creating a unique id for DragAndDrop
        SerialNumberStorage.push( ++SerialNumberStorage[(SerialNumberStorage.length -1)] )
        let serialNumber = SerialNumberStorage[(SerialNumberStorage.length -1)]
        const WIDGET_ID_NAME = "widgetElementNumber";
        component.setAttribute("id", (WIDGET_ID_NAME + serialNumber));
        const DRAGBLE = document.getElementById("drag-drop").checked;
        component.setAttribute('draggable', DRAGBLE);
        // create component
        component.innerHTML = `
        <div>
            <div class="task-widget__item-head" >
                <h4 class="task-widget__item-title" data-number="${number}">Накладная №${number}</h4>
        
                <div class="task-widget__item-wrapper">
                    <ul class="task-widget__item-move active">
                        <li class="item-static"><img src="files/icons/move.png" alt="icon_static"></li>
                        <li class="item-move"><img src="files/icons/move.png" alt="icon_move"></li>
                    </ul>
                    <ul class="task-widget__item-options">
                        <div class="task-widget__options-icon">
                            <img class="icon-menu" src="files/icons/menu.png" alt="icon-menu">
                        </div>
                        <ul class="task-widget__item-list">
                            <li data-redact="true">
                                <span class="icon"><img src="files/icons/edit.png" alt="icon-edit"></span>
                                <span class="description">Редактировать</span>
                            </li>
                            <li data-delete="true">
                                <span class="icon"><img src="files/icons/close.png" alt="icon-delete"></span>
                                <span class="description">Удалить</span>
                            </li>
                        </ul>
                    </ul>
                </div>
        
            </div>

            <ul class="task-widget__item-body">
                <li data-id="${id}">${id}</li>
                <li data-date="${timeMilisecond}">${timeToComponent}</li>
                <li data-type="${type}">${type}</li>
                <li data-from="${timeFrom}">${timeFrom}</li>
                <li data-to="${timeTo}">${timeTo}</li>
            </ul>
        </div>
        `;
        placeToPaste.append(component);
        itemAddDeleteHandler(component.children[0]);
        itemAddOptionsHandler(component);
        addDragEvent(component, placeToPaste);
        addChangeInformationHandler(component.children[0]);

        function itemAddDeleteHandler(component){
            let deleteButton = component.querySelector("ul.task-widget__item-list li[data-delete='true']");
            deleteButton.addEventListener("click", () => {
                deleteComponent(component);
            })

            function deleteComponent(component) {
                component.parentElement.remove();
            }
        }

        function itemAddOptionsHandler(component) {
            let menu = component.querySelector(".task-widget__item-options");
            let optionsMenu = menu.querySelector(".task-widget__item-list");
            menu.addEventListener("click", function(){
                this.classList.toggle("active");
            })
            optionsMenu.addEventListener("mouseleave", ()=>{
                menu.classList.remove("active");
            })
            document.addEventListener("click", (event)=>{
                let targetClass = event.target.classList.value? ("."+event.target.classList.value):false ;
                let onMenu = menu.querySelector(targetClass)? true:false;
                if ( !onMenu ) menu.classList.remove("active");
            })
        }

        function addDragEvent(component, placeToPaste) {
            component.addEventListener("dragstart", (event)=>{
                onDragStart(event);
            })

            function onDragStart(event) {
                let permissionDragStart = document.getElementById("drag-drop").checked;
                if (!permissionDragStart) return;

                event
                .dataTransfer
                .setData('text/plain', event.target.id);
            
                event
                .currentTarget
                .style
                .filter = 'blur(2px)';
            }

       
        }

        function addChangeInformationHandler(component){
            let redactButton = component.querySelector("li[data-redact]");
            redactButton.addEventListener("click", pasteRedactInput.bind(component))
            document.addEventListener("click", (event)=>{
                saveRedactValues(event, component)
            });

            function pasteRedactInput() {
                let componentValues = {
                    number : [this.querySelector(".task-widget__item-title[data-number]"),
                            this.querySelector(".task-widget__item-title[data-number]").innerText],
/*
                    id : [this.querySelector(".task-widget__item-body li[data-id]"),
                    this.querySelector(".task-widget__item-body li[data-id]").dataset.id],

                    date : [this.querySelector(".task-widget__item-body li[data-date]"),
                    this.querySelector(".task-widget__item-body li[data-date]").dataset.date],
*/
                    timeFrom : [this.querySelector(".task-widget__item-body li[data-from]"),
                    this.querySelector(".task-widget__item-body li[data-from]").dataset.from],
                    timeTo : [this.querySelector(".task-widget__item-body li[data-to]"),
                    this.querySelector(".task-widget__item-body li[data-to]").dataset.to],

                    type : [this.querySelector(".task-widget__item-body li[data-type]"),
                    this.querySelector(".task-widget__item-body li[data-type]").dataset.type], 
                }
               let keys = Object.keys(componentValues);
               for (let index = 0; index < keys.length; index++) {
                   const key = keys[index];
                   componentValues[key][0].innerHTML = `<input type="text" value="${componentValues[key][1]}">`;
               }
               this.classList.add("component-on-redact");
            }

            function saveRedactValues(event, component) {
                let componentClass = component.classList[0]
                let clickZone = event.target;

                while ( (clickZone.classList.value !== componentClass) &&
                        (clickZone.tagName !== "BODY") &&  
                        (clickZone.classList[0] !== "task-widget__item")){
                    clickZone = clickZone.parentNode;
                }

                if (clickZone.tagName !== "BODY") return;
                if (clickZone.classList.contains("task-widget__item")) return;
                if (!component.classList.contains("component-on-redact")) return;
                console.log("save");

                let componentValues = {
                    number : [component.querySelector(".task-widget__item-title[data-number]"),
                    component.querySelector(".task-widget__item-title[data-number] input").value,
                    "number"],
/*
                    id : [component.querySelector(".task-widget__item-body li[data-id]"),
                    component.querySelector(".task-widget__item-body li[data-id] input").value,
                    "id"],

                    date : [component.querySelector(".task-widget__item-body li[data-date]"),
                    component.querySelector(".task-widget__item-body li[data-date] input").value,
                    "date"],
*/
                    timeFrom : [component.querySelector(".task-widget__item-body li[data-from]"),
                    component.querySelector(".task-widget__item-body li[data-from] input").value,
                    "from"],
                    timeTo : [component.querySelector(".task-widget__item-body li[data-to]"),
                    component.querySelector(".task-widget__item-body li[data-to] input").value,
                    "to"],

                    type : [component.querySelector(".task-widget__item-body li[data-type]"),
                    component.querySelector(".task-widget__item-body li[data-type] input").value,
                    "type"], 
                }
               let keys = Object.keys(componentValues);
               for (let index = 0; index < keys.length; index++) {
                   const key = keys[index];

                   if (componentValues[key][2] === "number") {
                    componentValues[key][0].innerHTML = "Накладная №" + componentValues[key][1].replace(/\D/g, "");
                   componentValues[key][0]
                    .dataset[componentValues[key][2]] = componentValues[key][1].replace(/\D/g, "");
                   
                   } else {
                   componentValues[key][0].innerHTML = componentValues[key][1];
                   componentValues[key][0]
                    .dataset[componentValues[key][2]] = componentValues[key][1];
                   }
               }
               component.classList.remove("component-on-redact");
            }
        }
    }
 
    function onDragOver(event) {
        event.preventDefault();
    }
    
    function onDrop(event, placeToPaste) {
        let permissionDragStart = document.getElementById("drag-drop").checked;
        if (!permissionDragStart) return;

        const id = event
          .dataTransfer
          .getData('text/plain');
        const component = placeToPaste.querySelector(`li[id=${id}]`);
        component.style = "";
        let dropzone = event.target;

        while ( (dropzone.classList.value !== "task-widget__item") &&
                (dropzone.tagName !== "SECTION") && (dropzone.classList.value !== "task-widget__body-list")){
            dropzone = dropzone.parentNode;
        }
        
        if (dropzone.tagName !== "task-widget__body-list") {
            let componentsNodeList = placeToPaste.querySelectorAll("li.task-widget__item");
            let widgetNumberArray = [];

            for (const element of componentsNodeList) {
                widgetNumberArray.push(element.id.replace(/\D/g, ""));
            }
            let componentId = component.id.replace(/\D/g, "");
            let dropzoneId = dropzone.id.replace(/\D/g, "");
            //вычисляем порядковые номера компонентов
            let componentNumber = widgetNumberArray.indexOf(componentId, 0); 
            let dropzoneNumber = widgetNumberArray.indexOf(dropzoneId, 0);

            if ( (dropzoneNumber - componentNumber) === 1) {
                placeToPaste.insertBefore(component, dropzone.nextElementSibling);
            } else {
            placeToPaste.insertBefore(component, dropzone);
            }
            event.dataTransfer.clearData();
        }

    }

    function dragDropPermissionHandler(widgetBodyList) {
        let componentsList = widgetBodyList.querySelectorAll("li.task-widget__item");
        for (let index = 0; index < componentsList.length; index++) {
            const element = componentsList[index];
            element.setAttribute("draggable", this.checked)
        }
    }

    function addMask(component, matrix = "+7 (___) ___ ____", type = "number") {
        if (type === "number") {
        component.addEventListener("input", (e)=>{maskNumber(component, matrix,e)}, false);
        component.addEventListener("focus", (e)=>{maskNumber(component, matrix,e)}, false);
        component.addEventListener("blur", (e)=>{maskNumber(component, matrix,e)}, false);
        } else if (type === "string") {
            component.addEventListener("input", (e)=>{maskString(component, matrix,e)}, false);
            component.addEventListener("focus", (e)=>{maskString(component, matrix,e)}, false);
            component.addEventListener("blur", (e)=>{maskString(component, matrix,e)}, false);
        }

        function maskNumber(component, matrix = "+7 (___) ___ ____",event) {
            let i = 0,
                def = matrix.replace(/\D/g, ""),
                val = component.value.replace(/\D/g, "");

            if (def.length >= val.length) val = def;
            component.value = matrix.replace(/./g, function(a) {
                return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
            });
        
            if (event.type == "blur") {
                
                if (component.value.length == 2) return //component.value = ""
            } else setCursorPosition(component.value.length, component)
        
        };
        function maskString(component, matrix = "AA",event) {
            //nothing
        
        };
        function setCursorPosition(pos, elem) {
            elem.focus();
    
            if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
            else if (elem.createTextRange) {
                var range = elem.createTextRange();
                range.collapse(true);
                range.moveEnd("character", pos);
                range.moveStart("character", pos);
                range.select()
            }
        }
    }

    function createComponentFromStorage(componentList, SerialNumberStorage) {
        let objectList = localStorage.getItem(LOCAL_STORAGE_NAME);
        objectList = JSON.parse(objectList);
        for (const key in objectList) {
            if (Object.hasOwnProperty.call(objectList, key)) {
                const element = objectList[key];
                createItem(componentList,element.id,element.timeFrom,element.timeTo,element.number, element.type, SerialNumberStorage, element.time)
            }
        }
    }

    function saveComponentInStorage(componentList){
        let objectList = {};
        let components = componentList.querySelectorAll("li.task-widget__item");
        for(let i=0; i < components.length; i++) {
            objectList[i] = {
                id: components[i].querySelector("li[data-id]").dataset.id.replace(/\s/g, ""),
                time: components[i].querySelector("li[data-date]").dataset.date.replace(/\s/g, ""),
                timeFrom: components[i].querySelector("li[data-from]").dataset.from.replace(/\s/g, ""),
                timeTo: components[i].querySelector("li[data-to]").dataset.to.replace(/\s/g, ""),
                number: components[i].querySelector("h4[data-number]").dataset.number.replace(/\s/g, ""),
                type: components[i].querySelector("li[data-type]").dataset.type.replace(/\s/g, ""),
            }
        }
        objectList = JSON.stringify(objectList);
        localStorage.removeItem(LOCAL_STORAGE_NAME);
        localStorage.setItem(LOCAL_STORAGE_NAME, objectList);
    }
  
}

console.log(TaskWidjet.hex);