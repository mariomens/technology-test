const path = require("path");
const webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const FileManagerPlugin = require('filemanager-webpack-plugin'); 
const autoprefixer = require('autoprefixer');

const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
  mode: "development",
  context: path.resolve(__dirname, "src"),
  entry: {
    main: "./js/index.js",
    // img: './img/',
  },
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
  },

  resolve: {
    extensions: [".js", ".json", ".png", "svg", "gif", "jpeg", "jpg", "cur", "pptx"],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      "@": path.resolve(__dirname, "src"),
      "@models": path.resolve(__dirname, "src/models"),
    },
  },
  devServer: {
    port: 4200,
  },
  plugins: [
    new HTMLWebpackPlugin({ 
      template: "./index.html",
      minify: false,
    }),
    new VueLoaderPlugin(),
     /*
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
*/
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].bundle.css",
    }), 
    new CopyWebpackPlugin({
      patterns: [
        { from: "files", to: "files" }, 
        { from: "./favicon.ico", to: "./favicon.ico" }, 
      ],
    }),
    
  ],
  module: {
    rules: [ 
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
            loader: "babel-loader",
            options: {
              presets: ['@babel/preset-env']
            }
        }
    },

      {
        test: /\.hbs$/,                           //для обработки шаблонов
        include: path.resolve(__dirname, "src"), // old_ver include: path.resolve(__dirname, "src/template"),
        loader: "html-loader",
        options: {
          // Disables attributes processing
          minimize: false,
        }
      },

      {
        test: /\.css$/i,
        use: ['vue-style-loader', "css-loader","postcss-loader"],
      },
      {
        test: /\.less$/,
        exclude: /node_modules/,
        use: ['vue-style-loader', "css-loader",  'postcss-loader' ,  "less-loader"],
      },
      {
        test: /\.s[ac]ss$/i,
        use: ['vue-style-loader',
          { 
            loader: 'css-loader', 
            options: { sourceMap: true, }, 
          },  
          'postcss-loader' ,    
          { 
            loader: "sass-loader", 
            options: { sourceMap: true, },
          },
      ],
      }, 
      {
        test: /\.(png|svg|gif|jpg|jpeg|cur|pptx)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },
      {
        test: /\.(ttf|woff|woff2|eot|otf)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },
    ],
  },
};
